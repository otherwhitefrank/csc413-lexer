package lexer;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * The Lexer class is responsible for scanning the source file which is a stream
 * of characters and returning a stream of tokens; each token object will
 * contain the string (or access to the string) that describes the token along
 * with an indication of its location in the source program to be used for error
 * reporting; we are tracking line numbers; white spaces are space, tab,
 * newlines
 */
public class Lexer
{

    private boolean atEOF = false;
    private char ch;     // next character to process
    private SourceReader source;
    
    //Program logger
    private String programLog;

    // positions in line of current token
    //plus lineNumber
    private int startPosition, endPosition;

    public Lexer(String sourceFile) throws Exception
    {
        new TokenType();  // init token table
        
        //Initialize programLog passed from SourceReader
        programLog = "";
        
        //Stuck err check in for case of file not found
        try {
            
        source = new SourceReader(sourceFile);
        ch = source.read();
        } catch (IOException e)
        {
            System.out.println("IO Error opening source file!");
            System.exit(1);
        }
    }

    public static void main(String args[])
    {
        Token tok;
        try
        {
           
            String sep = System.getProperty("file.separator");
            String filePath = System.getProperty("user.dir") + sep + args[0];
            
            /* FILE DEBUG MESSAGES
            System.out.println("User's current working directory: " + System.getProperty("user.dir"));
            System.out.println("Number of arguments: " + args.length);
            System.out.println("Args: " + args);
            System.out.println("File Path: " + filePath);
            
            */ 
            
            Lexer lex = new Lexer(filePath);

            tok = lex.nextToken();
            while (tok != null)
            {

                //Changing this for prettier printing
                Symbol tokenKind = TokenType.tokens.get(tok.getKind());

                String tokId = "";

                if ((tok.getKind() == Tokens.Identifier)
                        || (tok.getKind() == Tokens.INTeger)
                        || (tok.getKind() == Tokens.FLOat))
                {
                    tokId += tok.toString();
                }
                else
                {
                    tokId += TokenType.tokens.get(tok.getKind());
                }

                System.out.printf("%s\tleft: %3d\tright: %3d\tline: %3d\n", tokId,
                        tok.getLeftPosition(), tok.getRightPosition(), tok.getLineNumber());

                tok = lex.nextToken();

            }
            
            //Output program source code
            System.out.println();
            System.out.println(lex.getProgramLog());
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        
        System.exit(0);
      

    }

    public String getProgramLog()
    {
        return programLog;
    }

    /**
     * newIdTokens are either ids or reserved words; new id's will be inserted
     * in the symbol table with an indication that they are id's
     *
     * @param id is the String just scanned - it's either an id or reserved word
     * @param startPosition is the column in the source file where the token
     * begins
     * @param endPosition is the column in the source file where the token ends
     * @return the Token; either an id or one for the reserved words
     */
    public Token newIdToken(String id, int startPosition, int endPosition)
    {
        //Added line number to token creation
        return new Token(startPosition, endPosition, source.getLineno(), Symbol.symbol(id, Tokens.Identifier));
    }

    /**
     * number tokens are inserted in the symbol table; we don't convert the
     * numeric strings to numbers until we load the bytecodes for interpreting;
     * this ensures that any machine numeric dependencies are deferred until we
     * actually run the program; i.e. the numeric constraints of the hardware
     * used to compile the source program are not used
     *
     * @param number is the int String just scanned
     * @param startPosition is the column in the source file where the int
     * begins
     * @param endPosition is the column in the source file where the int ends
     * @return the int Token
     */
    //Modified for lineNumber
    public Token newNumberToken(String number, int startPosition, int endPosition)
    {
        //Added line number to token creation
        return new Token(startPosition, endPosition, source.getLineno(),
                Symbol.symbol(number, Tokens.INTeger));
    }

    /**
     * build the token for operators (+ -) or separators (parens, braces) filter
     * out comments which begin with two slashes
     *
     * @param s is the String representing the token
     * @param startPosition is the column in the source file where the token
     * begins
     * @param endPosition is the column in the source file where the token ends
     * @return the Token just found
     */
    public Token makeToken(String s, int startPosition, int endPosition)
    {
        if (s.equals("//"))
        {  // filter comment
            try
            {
                int oldLine = source.getLineno();
                do
                {
                    ch = source.read();
                }
                while (oldLine == source.getLineno());
            }
            catch (Exception e)
            {
                atEOF = true;
            }
            return nextToken();
        }

        Symbol sym = Symbol.symbol(s, Tokens.BogusToken); // be sure it's a valid token
        if (sym == null)
        {
            System.out.println("******** illegal character: " + s + " at LineNumber: " + source.getLineno());
            atEOF = true;
            return nextToken();
        }
        return new Token(startPosition, endPosition, source.getLineno(), sym);
    }

    /**
     * @return the next Token found in the source file
     */
    public Token nextToken()
    { // ch is always the next char to process
        if (atEOF)
        {
            if (source != null)
            {
                //Before we close the SourceReader we need to retrieve the programLog
                this.programLog = source.getProgramLog();
                source.close();
                source = null;
            }
            return null;
        }
        try
        {
            while (Character.isWhitespace(ch))
            {  // scan past whitespace
                ch = source.read();
            }
        }
        catch (Exception e)
        {
            atEOF = true;
            return nextToken();
        }
        startPosition = source.getPosition();
        endPosition = startPosition - 1;

        if (Character.isJavaIdentifierStart(ch))
        {
            // return tokens for ids and reserved words
            String id = "";
            try
            {
                do
                {
                    endPosition++;
                    id += ch;
                    ch = source.read();
                }
                while (Character.isJavaIdentifierPart(ch));
            }
            catch (Exception e)
            {
                atEOF = true;
            }

            return newIdToken(id, startPosition, endPosition);
        }
        //Modified to check for '.' so we process cases like '0.52.1.3' as seperate tokens
        if (Character.isDigit(ch) || (ch == '.'))
        {
            //Sentinel for whether or not we have hit a decimel before
            boolean hitDecimal = false;
            //Modified to allow for Float type
            // return number tokens
            String number = "";
            try
            {
                do
                {
                    if (ch == '.')
                    {
                        hitDecimal = true;
                    }
                    endPosition++;
                    number += ch;
                    ch = source.read();

                }
                //Modified loop invariant to check to see if we hit a decimel after hitting one previously.
                while (Character.isDigit(ch) || ((ch == '.') && (hitDecimal == false)));
            }
            catch (Exception e)
            {
                atEOF = true;
            }
            
            //If we have hit a decimel and read another decimel there is an error so use
            //makeToken rather then newNumberToken to handle the improper symbol and generate an error
            //Our check is to see if our greedy algorithm returns a single . by itself.
            if (".".equals(number))
            {
                return makeToken(number, startPosition, endPosition);
            }
            else
            {
                return newNumberToken(number, startPosition, endPosition);
            }
            
        }

        // At this point the only tokens to check for are one or two
        // characters; we must also check for comments that begin with
        // 2 slashes
        String charOld = "" + ch;
        String op = charOld;
        Symbol sym;
        try
        {
            endPosition++;
            ch = source.read();
            op += ch;
            // check if valid 2 char operator; if it's not in the symbol
            // table then don't insert it since we really have a one char
            // token
            sym = Symbol.symbol(op, Tokens.BogusToken);
            if (sym == null)
            {  // it must be a one char token
                return makeToken(charOld, startPosition, endPosition);
            }
            endPosition++;
            ch = source.read();

            //Takes care of lineNumber internally
            return makeToken(op, startPosition, endPosition);
        }
        catch (Exception e)
        {
        }
        atEOF = true;
        if (startPosition == endPosition)
        {
            op = charOld;
        }
        //Takes care of lineNumber internally
        return makeToken(op, startPosition, endPosition);
    }
}
